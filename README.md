          <nav class="mega-menu">
            <ul class="mega-menu-dropdown" >
                
                <li>
                    <a>Estética y belleza</a>
                    <ul class="sub-menu-drop">

                       <ul>
                            <h3> Depilación</h3>
                                <li>Depilación eléctrica</li>
                                <li>Fotodepilación</li>
                           
                        </ul>
                                                <ul> 
                            <h3>Manos y Pies</h3>
                             <li>Manicura y pedicura</li>
                             <li>Uñas de gel</li>
                             <li>Limado y pintura</li> 
                        </ul>
                         <ul> 
                            <h3>Ojos y pestañas</h3>
                            <li>Lifting de pestañas </li>
                            <li>Tinte de cejas / pestañas</li>
                            <li>Diseño y depilación de cejas</li>
                            <li>Extensión y relleno de pestañas</li>
                        </ul>
                         <ul> 
                            <h3>Maquillaje</h3>
                            <li>Maquillaje de evento </li>
                            <li>Novio / Novia</li>
                            <li>Premamá</li>
                            <li>Fantasía / FX / Caracetrización</li>
                            <li>BodyPaint</li> 
                        </ul>
                    </ul>
                </li>
                
                <li>
                    <a>Tratamientos avanzados</a>
                    <ul class="sub-menu-drop">

                        <ul> 
                            <h3>Tratamientos faciales</h3>
                            <li>Higiene Detox </li>
                            <li>Anti Acné y seborregulador</li>
                            <li>Pure Vitamin C</li>
                            <li>Peeling ácidos</li>
                            <li>Programme Cellulaire Anti-âge </li>
                            <li>Remodelación de óvalo </li>
                            <li>Derma Pen</li>
                            <li>Absolue Revivre</li>
                            <li>Purificant</li>
                            <li>Caprices d'Or</li>
                        </ul>
                        <ul> 
                            <h3>Tratamientos corporales</h3>
                            <li>Reductor - Celulitis - Flacidez</li>
                            <li>Piernas cansadas</li>
                            <li>Tratamiento CaviSM</li>
                            <li>Masaje relax</li>
                            <li>Ritual piel de seda</li>
                            <li>Drenaje linfático manual</li>
                            <li>Masaje circulatorio</li>
                            <li>Envoltura de algas</li>
                            <li>Chocolaterapia</li>
                        </ul>

            
                    </ul>
                </li>
                
                
                 
                <li>
                    <a>Productos exclusivos</a>
                     
 
                </li>
                
                        
                <li>
                    <a>Ofertas</a>
                     
 
                </li>
                 
            </ul>
            
        </nav>
    
    
    
    .mega-menu {
        visibility: hidden;
        opacity: 0;  
        
        @include grid-media ($lg) {
           
            visibility: visible;
            opacity:1;
            @include size (100%,
            auto);
            background: #fff;
            top: 100px;
            position: relative;
            border-top: 1px solid #eaeaea;
            border-bottom: 1px solid #eaeaea;

        }


        .mega-menu-dropdown {
            text-align: center;


            li {
                display: block; 
                font-family: $font-one;
                font-weight: 500;
                font-size: .8em;
                text-transform: uppercase;
                letter-spacing: 2px;
                @include padding(1em);
                @include grid-media ($lg) {

                    display: inline-block;

                }

                &:hover {
                    

                    .sub-menu-drop { 
                        display:block;
                    }
                }

                /*-----------------optional background sleector----------------*/
                &:nth-child(1) {
                    .sub-menu-drop {

                        background: #eaeaea;
                        height: 700px;
                    }

                }
                &:nth-child(2) {
                    .sub-menu-drop {

                        background: #eaeaea;
                        height: 700px;
                    }

                }
                &:nth-child(3) {
                    .sub-menu-drop {

                        background: #eaeaea;
                        height: 700px;
                    }

                }


                .sub-menu-drop {
                    display:none;

                    @include grid-media($lg) {
                        @include size (100%,
                        100%);
                        position: absolute;
                        background: #eaeaea;
                        left: 0;
                        right: 0;
                        top: 43px;
                        float: none;
                    }

                    ul {
                        @include grid-column(3);
                        @include wrap;
                        text-align: left;
                    }
                }





            }

        }
    } 
    
    
        .show-menu {
        visibility:visible;
        opacity:1;
        
    }