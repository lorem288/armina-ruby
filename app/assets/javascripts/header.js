    //  dropdown menu responsive
    //  ____________________________

 $(document).ready(function() {
     $(".menu-item").click(function() {
         $(".dropdown-menu").slideUp(300);

         if (!$(this).next().is(":visible")) {
             $(this).next().slideDown(300);

         }
     });
 });
 
 