Rails.application.routes.draw do
   
  root to: 'pages#landing'
  
  get "estetica", to:'pages#estetica'
  get "tratamientos", to:'pages#tratamientos' 
  get "productos", to:'pages#productos'  
  get "ofertas", to:'pages#ofertas' 

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
